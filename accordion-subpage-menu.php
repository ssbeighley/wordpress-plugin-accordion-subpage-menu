<?php
/*
Plugin Name: 	Accordion Subpage Menu
Plugin URI: 	http://www.dozingdogdesign.com/plugins/accordion-subpage-menu/
Description: 	A widget to list subpages of a page in an accordion (expand/collapse) format, including parallel pages with subpages.
Version: 		1.5
Author: 		Dozing Dog
Author URI: 	http://www.dozingdogdesign.com/
License: 		GPLv2
*/

// Register script
add_action( 'wp_enqueue_scripts', 'my_scripts_method' );

function my_scripts_method() {
	wp_register_script('subpage-menu-js', plugins_url( '/js/appcode.js' , __FILE__ ), 'jquery', '1.0');
	wp_enqueue_script ( 'subpage-menu-js' );
}

// Register style sheet.
add_action( 'wp_enqueue_scripts', 'register_plugin_styles' );

function register_plugin_styles() {
	wp_register_style( 'subpage-css', plugins_url( 'accordion-subpage-menu/css/styles.css' ) );
	wp_enqueue_style( 'subpage-css' );
}


// Creating the widget 
class wpb_widget extends WP_Widget {

function __construct() {
parent::__construct(
// Base ID of your widget
'wpb_widget', 

// Widget name will appear in UI
__('LAHM Subpage Menu', 'wpb_widget_domain'), 

// Widget description
array( 'description' => __( 'Creates a nested list of subpages', 'wpb_widget_domain' ), ) 
);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );
// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];

// This is where you run the code and display the output

function get_top_parent_page_id() {
    global $post;
    $ancestors = $post->ancestors;
 
    // Check if page is a child page (any level)
    if ($ancestors) {
        //  Grab the ID of top-level page from the tree
        return end($ancestors);
    } else {
        // Page is the top level, so use  it's own id
        return $post->ID;
    }
}
$parent_page = get_top_parent_page_id($post->ID);
$parentTitle = get_the_title($parent_page);
$parentLink = get_permalink( $parent_page );

echo "<div class='subpage_menu_wrapper'><ul class='subpage-menu'>";
wp_list_pages("depth=2&child_of=$parent_page&title_li=<a href='$parentLink'><h4>$parentTitle</h4></a>&link_before=<span>&link_after=</span>");
echo "</ul></div>";
}
		
// Widget Backend 
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = __( 'New title', 'wpb_widget_domain' );
}
// Widget admin form
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}
} // Class wpb_widget ends here

// Register and load the widget
function wpb_load_widget() {
	register_widget( 'wpb_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );

?>