( function( $ ) {
  $( document ).ready(function() {
    $( '.subpage-menu ul li.current_page_ancestor' ).addClass('active'); // adds active class to parent section of page
	
    $( '.subpage-menu ul li a' ).click(function() {
      $('.subpage-menu ul li' ).removeClass('active');
      $(this).closest('li').addClass('active');	
      var checkElement = $(this).next();
      if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
        $(this).closest('li').removeClass('active');
        checkElement.slideUp('normal');
      }
      if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
        $('.subpage-menu ul ul:visible').slideUp('normal');
        checkElement.slideDown('normal');
      }
      if($(this).closest('li').find('ul').children().length == 0) {
        return true;
      } else {
        return false;	
      }		
    });
  });
} )( jQuery );
// JavaScript Document